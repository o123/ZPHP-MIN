# 单文件的极精简PHP框架（求赞，求星星）
## QQ群：592429541
### 此框架主体实现了：自动加载、控制器，和极简单的异常处理，没有其它多余功能。
### 框架主体预留了依赖加载功能，方便自己编写扩展功能
### 后续会提供：路由、debug、视图模板，数据库封装、图片验证码、文件上传 等扩展功能。可按需扩展
### 扩展后的功能，除了没有版本控制外将不弱于 [Z-PHP框架](http://www.z-php.com)
### 欢迎大家提交自己的扩展！

- **如果你有洁癖的话 ...**
- **如果你想自己扩展框架 ...**
- **如果你只是刚入门学习使用框架 ...**
- **如果你只是想要写几个简单的页面 ...**
- **如果你只是想写接口，不需要其它功能 ...**
- **如果你想要写一个完整的不算太复杂的程序 ...**

# 目录结构
```
 ├─ app  应用目录
 │  ├─ index   index 应用的目录
 │  │  ├─ ctrl   控制器目录
 │  │  ├─ model   模型目录
 │  │  ├─ lib   lib目录
 │  │  └─ config   配置文件目录
 │  │    ├─ config.php   配置文件
 │  │    ├─ mapping.php   映射文件
 │  │    └─ router.php   路由配置文件
 │  │
 │  └─ admin  (更多 应用的目录)
 │
 ├─ config  配置文件目录
 │  ├─ config.php   配置文件
 │  └─ mapping.php  映射文件
 │
 ├─ public  入口目录
 │  ├─ index.php   index 入口文件
 │  └─ admin.php  admin 入口文件
 │
 ├─ rely  依赖目录
 │
 ├─ libs  libs目录
 │
 └─ zphp-min.php   框架主体文件
```

# 最简目录结构
```
 ├─ app  应用目录
 │  ├─ index   index 应用的目录
 │  │  └─ ctrl   控制器目录
 │  │
 │  └─ admin  (更多 应用的目录)
 │
 ├─ public  入口目录
 │  ├─ index.php   index 入口文件
 │  └─ admin.php  admin 入口文件
 │
 └─ zphp-min.php   框架主体文件
```

# 开始使用
- ### 入口文件 (index.php)
```
<?php
define('APP_NAME', 'index'); /*定义应用目录名称*/
require '../zphp-min.php'; /*加载框架*/
AppRun(__FILE__);
```
- ### 控制器文件 (/app/index/ctrl/index.class.php)
```
<?php
namespace ctrl;
class index{
    static function index(){
        echo '<h1>ZPHP-MIN</h1>';
    }
}
```
# 更多说明请参考 [Z-PHP框架](http://www.z-php.com)

