<?php
// 全局配置
return [
    'DEBUG' => [
        'level' => 3,
        'type' => 'auto',
        'log' => 0,
    ],
    'ROUTER' => [
        'mod' => 2,
        'module' => false,
        'restfull' => null,
    ],
    'SESSION' => [
        'name' => 'SID', //session 名
        'auto' => true, //自动开启 session
        'redis' => false,
        'host' => '',
        'port' => '',
        'pass' => '',
    ],
    'DB' => [
        'dsn' => 'mysql:host=127.0.0.1;dbname=min2;port=3306',
        'db' => 'min2',
        'user' => 'root',
        'pass' => 'root',
        'charset' => 'utf8mb4',
        'prefix' => '',
    ],
];
